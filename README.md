Build script for install linux relevant packages and configuring a
conda/python environment.

### Usage
 * clone the repo
```sh
$ git clone git@bitbucket.org:gobrewers14/conda-linux-env-build.git
```
 * Make the script executable
```sh
$ chmod +x build.sh
```
 * Run it
```sh
$ ./build.sh
```
 * Once install finishes, source your bashrc.
```sh
$ source $HOME/.bashrc
```
 * Enjoy Python!
