#!/bin/bash

# install linux dependencies
sudo apt-get install vim curl fish git wget ssh openssl libssl-dev gcc g++ \
gfortran build-essential python-dev python-tk python-pip \
python-software-properties software-properties-common libopenblas-dev tk-dev \
libreadline-dev libfreetype6-dev zlib1g-dev make cmake automake autogen \
graphviz-dev libgraphviz-dev -y

# grab python from archive
wget http://repo.continuum.io/archive/Anaconda3-4.4.0-Linux-x86_64.sh -O anaconda3.sh

# create location for python env
mkdir $HOME/.localpython
bash anaconda3.sh -b -p $HOME/.localpython/conda

# use conda
export PATH="$HOME/.localpython/conda/bin:$PATH"

# install prelems
conda config --set always_yes yes
conda info -a

# create env
conda env create -f env.yml
export PATH="$HOME/.localpython/conda/envs/socketio/bin:$PATH"

# activate env
source activate socketio

# install env specific packages
conda install nb_conda
pip install --upgrade jupyter
python -m spacy download en

# add path to bashrc and source 
echo 'export PATH="$HOME/.localpython/conda/bin:$PATH"' >> $HOME/.bashrc
echo "finished"
